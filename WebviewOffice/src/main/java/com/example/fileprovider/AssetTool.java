package com.example.fileprovider;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * http://stackoverflow.com/questions/4447477/android-how-to-copy-files-in- assets-to-sdcard description:
 * <p/>
 * asset相关工具方法
 *
 * @date: 2014年5月7日
 * @author: wzq
 */
public class AssetTool {
    private static final String tag = AssetTool.class.getSimpleName();

    private Context mCtx;
    private String mRootDir;

    /**
     * 将assets目录整个拷贝到另外一个目录
     *
     * @param ctx
     * @param rootDir
     */
    public void copyAsset(Context ctx, String rootDir) {
        mCtx = ctx;
        mRootDir = rootDir;
        copyFileOrDir("");
    }

    private void copyFileOrDir(String path) {
        AssetManager assetManager = mCtx.getAssets();
        String assets[] = null;
        try {
            assets = assetManager.list(path);
            // TODO 如果path="",则列举根目录。根目录下有 webkit sounds images是系统目录
            // 这些目录我们不需要
            if("".equals(path)) {
                ArrayList<String> list = new ArrayList<String>();
                List<String> filterList = new ArrayList<String>();
                filterList.add("webkit");
                filterList.add("sounds");
                // filterList.add("images");
                for(String s : assets) {
                    if(!filterList.contains(s)) {
                        list.add(s);
                    }
                }
                assets = new String[list.size()];
                list.toArray(assets);
            }
            if(assets.length == 0) {
                copyFile(path);
            } else {
                // dir
                File dir = new File(mRootDir, path);
                if(!dir.exists()) {
                    dir.mkdir();
                }
                for(int i = 0; i < assets.length; ++i) {
                    // asset的根目录列举是用copyFileOrDir(""),根目录下的目录不用"/"
                    String folderPath;
                    if("".equals(path)) {
                        folderPath = assets[i];
                    } else {
                        folderPath = path + "/" + assets[i];
                    }
                    copyFileOrDir(folderPath);
                }
            }
        } catch(IOException ex) {
            Log.e(tag, ex.toString());
            ex.printStackTrace();
        }
    }

    private void copyFile(String filename) {
        AssetManager assetManager = mCtx.getAssets();
        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(filename);
            String newFileName = mRootDir + "/" + filename;
            // TODO:删除之前的
            File file = new File(newFileName);
            if(file.exists()) {
                file.delete();
            }
            // TODO:加密后保存
            // in = CryptToolFactory.getInstance().encryptStream(in);
            out = new FileOutputStream(newFileName);
            byte[] buffer = new byte[1024];
            int read;
            while((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch(Exception ex) {
            Log.e(tag, ex.toString());
            ex.printStackTrace();
        }
    }
}
