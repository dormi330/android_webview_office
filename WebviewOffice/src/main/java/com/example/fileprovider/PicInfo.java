package com.example.fileprovider;

public class PicInfo {
    private static final String DEFAULT_PIC_NAME = "0.jpg";
    private String sheetName;//表名称
    private int colNum;//列号
    private int rowNum;//行号
    private String picName = DEFAULT_PIC_NAME;//图片名称
    private byte[] content;


    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }
    public void setColNum(int colNum) {
        this.colNum = colNum;
    }
    public void setPicName(String picName) {
        this.picName = picName;
    }
    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }
    public void setContent(byte[] bytes) {
        this.content = bytes;
    }

    public String getSheetName() {
        return sheetName;
    }
    public int getRowNum() {
        return rowNum;
    }
    public int getColNum() {
        return colNum;
    }
    public byte[] getContent() {
        return content;
    }
    public String getPicName() {
        return picName;
    }
}
