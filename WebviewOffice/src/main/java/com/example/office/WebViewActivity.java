package com.example.office;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
/**
 * @author: wzq
 * @date: 14-6-26
 * description: change it at File | setting | File and code templates | include | file header
 */
public class WebViewActivity extends Activity {
    protected final String TAG = this.getClass().getSimpleName();
    protected static final String SD_ROOT = Environment.getExternalStorageDirectory().getAbsolutePath();
    protected WebView webview;
    protected Context mCtx;
    protected Button btnLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCtx = this;
        setContentView(R.layout.act_load);
        webview = (WebView) findViewById(R.id.wv_web1);
        btnLoad = (Button) findViewById(R.id.btn_load);

        //
        initWebview();
    }


    private void initWebview() {
        webview.getSettings().setJavaScriptEnabled(true);// allows js
        webview.setWebChromeClient(new WebChromeClient());// allows js alert windows
        webview.setWebViewClient(new WebViewClient());// 设置WebView客户端对象
    }
}
