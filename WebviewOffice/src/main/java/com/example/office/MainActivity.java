package com.example.office;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import com.example.fileprovider.AssetTool;
import com.example.office.doc.WebviewLoadDoc3;
import com.example.office.docx.WebviewLoadDocx;
import com.example.office.xls.WebviewLoadXls;
import com.example.office.xls.WebviewLoadXlsx;


public class MainActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);
        new AssetTool().copyAsset(this, Environment.getExternalStorageDirectory() + "/_test");

        findViewById(R.id.btn_read_doc).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                goActivity(WebviewLoadDoc3.class);
            }
        });

        findViewById(R.id.btn_read_docx).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                goActivity(WebviewLoadDocx.class);
            }
        });

        findViewById(R.id.btn_read_xls).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                goActivity(WebviewLoadXls.class);
            }
        });

        findViewById(R.id.btn_read_xlsx).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                goActivity(WebviewLoadXlsx.class);
            }
        });
    }

    private void goActivity(Class<? extends Activity> goClass) {
        Intent intent = new Intent(this, goClass);
        startActivity(intent);
    }
}
