package com.example.fileprovider;
/**
 * project:
 * author: wzq
 * date: 2014/6/30
 * description:
 */
import org.apache.poi.hwpf.HWPFDocumentCore;
import org.apache.poi.hwpf.converter.WordToHtmlConverter;
import org.apache.poi.hwpf.converter.WordToHtmlUtils;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
/**
 * doc to html
 */
public class Doc2Html implements Htmlable {

    private static final String TAG = Doc2Html.class.getSimpleName();
    private String cachePrefix;
    private String folder;
    private String docFileName;
    private InputStream inputStream;

    @Override
    public InputStream getHtmlInputStream() {
        return inputStream;
    }

    public Doc2Html(String docName) {
        this.docFileName = docName;
        folder = docName.substring(0, docName.lastIndexOf("/"));
        cachePrefix = getFileNameWithoutPathAndSuffix(docName);
    }

    /**
     * 获取文件名 不包含 路径 和 后缀 如 /mnt/sdcard/../a_doc.html => a_doc
     *
     * @param docName
     *
     * @return
     */
    private static String getFileNameWithoutPathAndSuffix(String docName) {
        int index1 = docName.lastIndexOf("/") + 1;
        int index2 = docName.lastIndexOf(".");
        if(index2 < index1) {
            index2 = docName.length();
        }

        return docName.substring(index1, index2);
    }

    /**
     * 将word文档转换为html文件
     */
    @Override
    public boolean convertToHtml() {
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream(docFileName);
            HWPFDocumentCore hwpfDocument = WordToHtmlUtils.loadDoc(fileInputStream);
            Document newDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            WordToHtmlConverter wordToHtmlConverter = new WordToHtmlConverter(newDocument);
            wordToHtmlConverter.setPicturesManager(new OfficePicMgr(folder, cachePrefix));
            wordToHtmlConverter.processDocument(hwpfDocument);
            StringWriter stringWriter = new StringWriter();
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
            transformer.setOutputProperty(OutputKeys.METHOD, "html");
            transformer.transform(new DOMSource(wordToHtmlConverter.getDocument()), new StreamResult(stringWriter));
            byte[] bytes = stringWriter.toString().getBytes();
            inputStream = new ByteArrayInputStream(bytes);

            fileInputStream.close();
            return true;
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        } catch(ParserConfigurationException e) {
            e.printStackTrace();
        } catch(TransformerConfigurationException e) {
            e.printStackTrace();
        } catch(TransformerFactoryConfigurationError e) {
            e.printStackTrace();
        } catch(TransformerException e) {
            e.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
