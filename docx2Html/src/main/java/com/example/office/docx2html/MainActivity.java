package com.example.office.docx2html;//package com.example.office.docx2html;
//
//import android.app.Activity;
//import android.os.Bundle;
//import android.os.Environment;
//import android.webkit.WebView;
//import org.docx4j.convert.out.html.AbstractHtmlExporter;
//import org.docx4j.convert.out.html.AbstractHtmlExporter.HtmlSettings;
//import org.docx4j.convert.out.html.HtmlExporterNG2;
//import org.docx4j.model.images.AbstractWordXmlPicture;
//import org.docx4j.model.images.ConversionImageHandler;
//import org.docx4j.openpackaging.exceptions.Docx4JException;
//import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
//import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPart;
//import org.docx4j.relationships.Relationship;
//
//import javax.xml.transform.stream.StreamResult;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.nio.ByteBuffer;
//
//
//public class MainActivity extends Activity {
//    WebView webView;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        webView = (WebView) findViewById(R.id.webview);
//        webView.getSettings().setJavaScriptEnabled(true);
//
//        // setting
//        final String dir = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "_test";
//        final String docFile = dir + "/test.docx";
//        final String imgFolder = dir + "/html/images/ooxml/";
//        final String htmlFile = dir + "/html/test_docx.html";
//
//        try {
//            long start = System.currentTimeMillis();
//
//            // 1) Load DOCX into WordprocessingMLPackage
//            InputStream is = new FileInputStream(new File(docFile));
//            WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(is);
//
//            // 2) Prepare HTML settings
//            HtmlSettings htmlSettings = new HtmlSettings();
//            htmlSettings.setImageHandler(new ConversionImageHandler() {
//                @Override
//                public String handleImage(AbstractWordXmlPicture abstractWordXmlPicture, Relationship relationship,
//                                          BinaryPart binaryPart) throws Docx4JException {
//                    try {
//                        // Extract image
//                        String name = relationship.getId();
//                        File imgFile = new File(imgFolder, name);
//
//                        OutputStream out = new FileOutputStream(imgFile);
//                        ByteBuffer byteBuffer = binaryPart.getBuffer();
//
//                        byte[] content = new byte[byteBuffer.remaining()];
//                        byteBuffer.get(content);
//                        if(content != null && content.length > 0) {
//                            out.write(content, 0, content.length);
//                        }
//                        return imgFile.getAbsolutePath();
//                    } catch(FileNotFoundException e) {
//                        e.printStackTrace();
//                    } catch(IOException e) {
//                        e.printStackTrace();
//                    }
//
//                    return null;
//                }
//            });
//
//            // 3) Convert WordprocessingMLPackage to HTML
//            OutputStream out = new FileOutputStream(new File(htmlFile));
//            AbstractHtmlExporter exporter = new HtmlExporterNG2();
//            StreamResult result = new StreamResult(out);
//            exporter.html(wordMLPackage, result, htmlSettings);
//
//            System.err.println("Generate html/ooxml.html with " + (System.currentTimeMillis() - start) + "ms");
//
//        } catch(Throwable e) {
//            e.printStackTrace();
//        }
//
//        // load in webview
//
//        webView.loadUrl("file:///" + htmlFile);
//    }
//}
