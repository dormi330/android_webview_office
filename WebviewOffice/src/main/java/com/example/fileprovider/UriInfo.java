package com.example.fileprovider;
/**
 * project:
 * author: wzq
 * date: 2014/7/1
 * description: 解析uri
 */
public class UriInfo {
    private final String scheme;
    private final String authority;
    private final String fullPath;

    private String fileDir; //文件路径
    private String fileName; // 文件名称
    private String filePostfix;//文件后缀名

    public UriInfo(String uri) {
        int index1 = uri.indexOf("://") + 3;
        scheme = uri.substring(0, index1);

        int index2 = uri.indexOf("/", index1) + 1;
        authority = uri.substring(index1, index2);
        fullPath = uri.substring(index2);

        // 将filePath分解
        int index3 = fullPath.lastIndexOf("/");
        int index4 = fullPath.lastIndexOf(".");
        fileDir = fullPath.substring(0,index3);
        fileName = fullPath.substring(index3+1,index4);
        filePostfix = fullPath.substring(index4+1,fullPath.length());
    }

    public void check() {
        System.out.println("scheme = " + scheme);
        System.out.println("authority = " + authority);
        System.out.println("fullPath = " + fullPath);

        System.out.println("fileDir = " + fileDir);
        System.out.println("fileName = " + fileName);
        System.out.println("filePostfix = " + filePostfix);
    }
    public String getScheme() {
        return scheme;
    }
    public String getAuthority() {
        return authority;
    }
    public String getFullPath() {
        return fullPath;
    }
    public String getFileDir() {
        return fileDir;
    }
    public String getFileName() {
        return fileName;
    }
    public String getFilePostfix() {
        return filePostfix;
    }


    public static void main(){
        UriInfo uriInfo = new UriInfo("content://com.foss.abc/mnt/sdcard/_test/aa.doc");
        uriInfo.check();
    }
}
